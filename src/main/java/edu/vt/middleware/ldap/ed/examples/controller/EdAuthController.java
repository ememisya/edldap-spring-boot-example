package edu.vt.middleware.ldap.ed.examples.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Middleware Services
 */
@CrossOrigin(allowCredentials = "false", origins = "*")
@RestController
public class EdAuthController
{

  @RequestMapping(value = "/auth", method = RequestMethod.GET)
  @PreAuthorize("hasAuthority('VT-EMPLOYEE')")
  public ResponseEntity<String> auth(final Authentication authentication)
  {
    return new ResponseEntity<>("Logged in as VT-EMPLOYEE: " + authentication.getPrincipal().toString(), HttpStatus.OK);
  }

}
