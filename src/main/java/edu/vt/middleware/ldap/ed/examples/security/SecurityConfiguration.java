package edu.vt.middleware.ldap.ed.examples.security;

import edu.vt.middleware.ldap.ed.spring.EdAuthAuthenticationProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

/**
 * @author Middleware Services
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{

  @Bean
  public BasicAuthenticationEntryPoint edAuthAuthenticationProvider() {
    final BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
    entryPoint.setRealmName("EdAuthAuthenticationProvider");
    return entryPoint;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception
  {
    http
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .csrf().disable()
            .rememberMe().disable()
            .requiresChannel()
            .anyRequest()
            .requiresSecure()
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .permitAll()
            .antMatchers("/auth")
            .fullyAuthenticated().and()
            .authenticationProvider(new EdAuthAuthenticationProvider())
            .httpBasic()
            .authenticationEntryPoint(edAuthAuthenticationProvider());
  }

}
