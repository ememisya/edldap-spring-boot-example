# edldap-spring-boot-example

Spring Boot example demonstrating usage of EdAuthAuthenticationProvider

This application is meant to demonstrate the use of EdAuthAuthenticationProvider only.

Step 1 *Setup your keystore*
```
keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -storepass changeit -validity 360 -keysize 2048
```
Step 2 *Update src/main/resources/application.properties*
```
server.port=8443
server.ssl.key-store=/apps/private/keystore.jks
server.ssl.key-store-password=changeit
server.ssl.keyStoreType=JKS
server.ssl.keyAlias=selfsigned
```

Step 3 *Run application*

Step 4 *Navigate to https://localhost:8443/auth*